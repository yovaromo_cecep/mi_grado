function acciones(){   
    $("#managment").addClass('menu-open');
    var page = window.location.pathname;
    
    if( page.indexOf('/',2) == -1){
        page = page.substring(1);  
    } else{
        page = page.substring(1,page.indexOf('/',2));  
    }
    
    $("#"+page).addClass('active');
    $("#page_current").html(page.substring(0,1).toUpperCase()+page.substring(1));

    $(document.body).tooltip({ 
        selector: "[title]"
    });

}

$(document).ready(acciones);

