function accionesDocument(){   

    var dt = $("#tabla").DataTable({
        "language": {
            "emptyTable": "Documentos no Registrados"
        },
        "processing": true,
        "serverSide": true,
        "ajax": "api/document",
        "columns": [
            { "data": "id"} ,
            { "data": "name"} ,
            { "data": "description" },
            { "data": "id",
                render: function (data) {
                    return  '<a href="/document/'+data+'/edit" title="Modificar información del documento" class="btn btn-primary mr-2" ><i class="fa fa-edit"></i></a>'+                                
                            '<a href="" data-id ="'+data+'" title="Eliminar el documento" class="borrar btn btn-danger"><i class="fa fa-trash"></i></a>'
                }
            }
        ]
    });

    $("#tabla").on('click','a.borrar',(function(e){
        e.preventDefault();
        swal({
            title: '¿Está seguro?',
            text: "No podrá revertir este proceso!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, Eliminar!'
        })                    
        .then((result) => {
            if (result.value) {
                let id = $(this).data("id");
                let url = "/document/"+id;
                let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

                var request = $.ajax({
                    url: url,
                    method: "DELETE",
                    data: { _token : token },
                    dataType: "json"
                });
                   
                request.done(function( resultado ) { 
                    if(resultado.respuesta == 'correcto'){
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'El registro fue eliminado! ',
                            showConfirmButton: false,
                            timer: 1500
                        })    
                        dt.ajax.reload(null, false);
                    } else {
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Ocurrió un error!'                         
                        })
                    }
                });
                   
                request.fail(function( jqXHR, textStatus ) {
                    swal({
                        type: 'error',
                        title: 'No está autorizado...',
                        text: 'Ocurrió un error!'                         
                    })
                 });
            }
        })
    })
    );  


    $.validate({
        form : '#frm-grabar',
        onSuccess : function($form) {
            const datos = $("#frm-grabar").serialize();         
            var jqxhr = 
                $.post( "/document", datos)          
                .done(function(resultado) {
                    if(resultado.respuesta == 'correcto'){
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'El registro se guardó con éxito ',
                            showConfirmButton: false,
                            timer: 1500,
                            
                        }).then(() => {
                            window.history.back();
                        });              
                    } else {
                        if(resultado.status === 422) {
                            var errors = resultado.responseJSON;
                            $.each(errors, function (key, value) {
                                $('.'+key+'-error').html(value[0]);  
                                $('.'+key+'-error').removeAttr('hidden');
                                $('.'+key+'-error').prev().addClass('error');
                            });
                        }
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Hubo un error al grabar'                   
                        })
                    }
            })
            .fail(function(error) {
                if(error.status === 422) {
                    var errors = error.responseJSON;
                    $.each(errors, function (key, value) {
                        $('.'+key+'-error').html(value[0]);  
                        $('.'+key+'-error').removeAttr('hidden');
                        $('.'+key+'-error').prev().addClass('error');
                    });
                }
                swal({
                    position: 'top-end',
                    type: 'error',
                    title: 'Hubo errores desde el servidor ',
                    showConfirmButton: false,
                    timer: 1500,                    
                })
            })
        }
      });

    $("#bnt-grabar").click(function(e){
        e.preventDefault();
        $("#bnt-grabar").submit();                    
    });

    $("#bnt-actualizar").click(function(e){
        e.preventDefault();
        const datos = $("#frm-editar").serialize();      
        let id = $("#id").val();
        let url = "/document/"+id;
        let token = document.querySelector('input[name="_token"]').getAttribute('value');

        var request = $.ajax({
            url: url,
            method: "PUT",
            data: datos,
            dataType: "json"
        });

        request.done(function( resultado ) {             
            if(resultado.respuesta == 'correcto'){
                swal({
                    position: 'top-end',
                    type: 'success',
                    title: 'El registro fue actualizado! ',
                    showConfirmButton: false,
                    timer: 1500
                }).then(() => {
                    window.history.back();
                })
                
            } else {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Ocurrió un error!'                         
                })
            }
        });

        request.fail(function( jqXHR, textStatus ) {
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Ocurrió un error!'                         
            })
         });

    });
}

$(document).ready(accionesDocument);

