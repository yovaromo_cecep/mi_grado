<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
<<<<<<< HEAD
use Validator;

class DocumentController extends Controller
{
    public function __Construct(){        
        $this->middleware('auth');             
    }
    
=======

class DocumentController extends Controller
{
>>>>>>> 9f88a3fbc580bda6b9fa0de351308f129b5e6fec
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
<<<<<<< HEAD
        $this->authorize('isAdmin');           
=======
>>>>>>> 9f88a3fbc580bda6b9fa0de351308f129b5e6fec
        $documents = Document::all();
        return view('Document.index',compact('documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Document.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
<<<<<<< HEAD
    public function store(Request $request)    
    {
        $validacion = Validator::make($request->all(), [
            'name' => 'required|alpha_num|max:100',
            'description' => 'required']);

        if($validacion->fails()) {    
            return response()->json($validacion->messages(), 422);
        } else {
            $document = Document::create($request->all());        
            return response()->json(['respuesta' => 'correcto']);
         }
=======
    public function store(Request $request)
    {
        $document = Document::create($request->all());        
        return response()->json(['respuesta' => 'correcto']);
>>>>>>> 9f88a3fbc580bda6b9fa0de351308f129b5e6fec
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::findOrFail($id);
        return view('Document.edit',compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $document = Document::findOrFail($id);
        $document->fill($request->all());
        $document->save();
        return response()->json(['respuesta' => 'correcto']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
<<<<<<< HEAD

=======
>>>>>>> 9f88a3fbc580bda6b9fa0de351308f129b5e6fec
        $document = Document::findOrfail($id);
        $document->delete();
        return response()->json(['respuesta' => 'correcto']);
    }
}
