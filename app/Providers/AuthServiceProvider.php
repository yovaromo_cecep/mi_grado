<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

<<<<<<< HEAD
        Gate::define('isAdmin', function($user){
            return (strtolower($user->type) === 'admin');
        });

        Gate::define('isUser', function($user){
            return (strtolower($user->type) === 'user');
        });

=======
        //
>>>>>>> 9f88a3fbc580bda6b9fa0de351308f129b5e6fec
    }
}
