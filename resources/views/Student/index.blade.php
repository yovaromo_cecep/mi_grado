@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Lista de Studiantes</h3>
                    <div class="card-tools">
                        <button class="btn btn-success">Add New<i class="fa fa-user-plus fa-fw"></i></button>
                    </div>
                </div>
                @include('commons.success')
                <div class="card-body">
                   <table>
                   <thead>
                    <tr>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    
@endsection