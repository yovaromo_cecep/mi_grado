@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Lista de Documentos</h3>
                    <div class="card-tools">
                        <a href="{{route('document.create')}}" title="Registrar un nuevo Documento" class="btn btn-success">Registrar<i class="fas fa-plus-circle fa-lg ml-3"></i></a>
                    </div>                    
                </div>
                @include("commons.success")
                <div class="card-body">
                   <table id="tabla" class="table table-striped">
                   <thead>
                    <tr>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Modificar</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="/js/documents.js"></script>
@endsection