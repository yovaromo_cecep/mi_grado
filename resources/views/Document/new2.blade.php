@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
        <div class="card card-primary mt-3">
            <div class="card-header">
                <h3 class="card-title">Nuevo Documento</h3>                    
            </div>

            
            <!-- /.card-header -->
            <!-- form start -->
            <form id = "frm-grabar" class="form-horizontal" method="post" action="{{route('document.show')}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label font-weight-bold">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" 
                                placeholder="Name" name="name">
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label font-weight-bold">Descripción</label>
                        <div class="col-sm-10">
                        <textarea cols="50" rows="3" class="form-control" id="description" placeholder="Descripción" 
                            name="description"
                            ></textarea>
                        </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                <input id="bnt-grabar2" type="submit" title="Graba el nuevo documento" class="btn btn-primary" value="Guardar">
                <a href="/document" title="Retorna al listado de documentos" class="btn btn-danger float-right">Cancelar</a>
                </div>
                <!-- /.card-footer -->
            </form>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    
@endsection

