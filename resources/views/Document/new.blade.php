@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
        <div class="card card-primary mt-3">
            <div class="card-header">
                <h3 class="card-title">Nuevo Documento</h3>                    
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id = "frm-grabar" class="form-horizontal" method="post" action="">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label font-weight-bold">Nombre</label>
                        <div class="col-sm-10">
<<<<<<< HEAD
                            <input type="text" class="form-control" id="name" 
                                placeholder="Name" name="name"
                                data-validation="alphanumeric"
                                data-validation="required" > 
                                <span class="text-danger name-error mt-5" hidden></span>    
                                                  
=======
                            <input type="text" class="form-control" id="name" placeholder="Name" name="name">
>>>>>>> 9f88a3fbc580bda6b9fa0de351308f129b5e6fec
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label font-weight-bold">Descripción</label>
                        <div class="col-sm-10">
<<<<<<< HEAD
                            <textarea cols="50" rows="3" class="form-control" id="description" placeholder="Descripción" 
                                name="description"
                                data-validation="required" ></textarea>
                                <span class="text-danger description-error mt-5" hidden></span>
=======
                        <textarea cols="50" rows="3" class="form-control" id="description" placeholder="Descripción" 
                            name="description"></textarea>
>>>>>>> 9f88a3fbc580bda6b9fa0de351308f129b5e6fec
                        </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                <button id="bnt-grabar" title="Graba el nuevo documento" class="btn btn-primary">Guardar</button>
                <a href="/document" title="Retorna al listado de documentos" class="btn btn-danger float-right">Cancelar</a>
                </div>
                <!-- /.card-footer -->
            </form>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="/js/documents.js"></script>
@endsection

