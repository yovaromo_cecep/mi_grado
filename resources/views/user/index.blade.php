@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Lista de Usuarios</div>

                <div class="card-body">
                   <table>
                   <thead>
                    <tr><th>Name</th></tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr><td>{{$user->name}}</td></tr>
                        @endforeach
                    </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection