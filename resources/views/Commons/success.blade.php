@if(session('status'))

	@if(session('status') == 'eliminado')
		<script type="text/javascript">
			swal({
				position: 'top-end',
				type: 'success',
				title: 'The record was deleted',
				showConfirmButton: false,
				timer: 2000
			})
		</script>
	@endif

	@if(session('status') == 'guardado')
		{{session('status')}}
		<script type="text/javascript">
		console.log("bien");

		swal({
			position: 'top-end',
			type: 'success',
			title: 'Registro almacenado',
			showConfirmButton: false,
			timer: 2000
		})
		</script>
	@endif

	@if(session('status') == 'actualizado')
		<script type="text/javascript">
			swal({
				position: 'top-end',
				type: 'success',
				title: 'The record was updated',
				showConfirmButton: false,
				timer: 2000
			})
		</script>
	@endif
@endif